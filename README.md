# README.md Template

Source: <https://bitbucket.org/cps491s19-team0/cps491s19.git>

*NOTE*: _This is just a tentative template for your team to start working on sprint 0. It is a minimum requirement for your project final report and can be updated later.
Your team can revise/add more sections, however, it is highly recommended to seek approval from the instructor for a pull request._

University of Dayton

Department of Computer Science

CPS 491 - Capstone II, Spring 2019

Instructor: Dr. Phu Phung


## Capstone II Project 


# Project Topic/Title


# Team members

1.  Member 1, email
2.  Member 2, email
3.  Member 3, email
4.  Member 4, email


# Company Mentors

Mentor 1, _title_; Mentor 2, _title_; ... 

Company name

Company address


# Project Management Information

Management board (private access): <https://trello.com/***>

Source code repository (private access): <https://bitbucket.org/***>

Project homepage (public): <https://cps491s19-team0.bitbucket.io>

## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
| 1/21/2019|  0.0          | Init draft   |


# Overview

Describe the overview of the project with a high-level architecture figure. 
_Note: For sprint 0, you can copy the content from your project proposal in CPS 490. You need to update this section according to in each sprint_ 

# Project Context and Scope

Describe the context where the project will be used or deployed and the scope of the project your team will develop
_Note: For sprint 0, you can copy the content from your project proposal in CPS 490. You need to update this section according to in each sprint_ 

# System Analysis

_(Start from Sprint 0, keep updating)_

## High-level Requirements

List high-level requirements of the project that your team will develop into use cases in later steps

## Use cases

Draw the overview use case diagram, and define use case description _(Main focus of Sprint 0)_

# System Design

_(Start from Sprint 1, keep updating)_

## Use-Case Realization

## Database 

## User Interface


# Implementation

_(Start from Sprint 1, keep updating. However, it is important to prepare the technology from Sprint 0)_

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 


## Deployment

Describe how to deploy your system in a specific platform.

# Software Process Management

_(Start from Sprint 0, keep updating)_

Introduce how your team uses a software management process, e.g., Scrum, how your teamwork, collaborate.

Include the Trello board with product backlog and sprint cycles in an overview figure and also in detail description. _(Main focus of Sprint 0)_

Also, include the Gantt chart reflects the timeline from the Trello board. _(Main focus of Sprint 0)_


## Scrum process

### Sprint 0

Duration: 1/15/2019-1/28/2019

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...

#### Contributions: 

1.  Member 1, x hours, contributed in xxx
2.  Member 2, x hours, contributed in xxx
3.  Member 3, x hours, contributed in xxx
4.  Member 4, x hours, contributed in xxx

### Sprint 1

Duration: 1/29/2019-2/13/2019

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...

#### Contributions: 

1.  Member 1, x hours, contributed in xxx
2.  Member 2, x hours, contributed in xxx
3.  Member 3, x hours, contributed in xxx
4.  Member 4, x hours, contributed in xxx

#### Sprint Retrospective:

_(Introduction to Sprint Retrospective:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |


### Sprint ...

Duration: MM/DD/YYYY-MM/DD/YYYY

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...
#### Contributions: 

1.  Member 1, x hours, contributed in xxx
2.  Member 2, x hours, contributed in xxx
3.  Member 3, x hours, contributed in xxx
4.  Member 4, x hours, contributed in xxx

#### Sprint Retrospective: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |


# User guide/Demo

Write as a demo with screenshots and as a guide for users to use your system.

(Start from Sprint 1, keep updating)

# Acknowledgments 

You can thank the company that sponsors your project.